import {shallow, ShallowWrapper} from 'enzyme';
import React from 'react';
import {TechnologyTile} from '../../src/components/TechnologyTile';

describe('TechnologyTile', (): void => {
	it('renders', (): void => {
		//when
		const wrapper: ShallowWrapper = shallow(<TechnologyTile name='test' image={<></>}/>);

		//then
		expect(wrapper).toMatchSnapshot();
	});
});
