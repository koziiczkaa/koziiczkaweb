import {shallow, ShallowWrapper} from 'enzyme';
import React from 'react';
import {Button} from '../../src/components/Button';

describe('Button', (): void => {
	it('renders', (): void => {
		//when
		const wrapper: ShallowWrapper = shallow(<Button name='test'/>);

		//then
		expect(wrapper).toMatchSnapshot();
	});
});
