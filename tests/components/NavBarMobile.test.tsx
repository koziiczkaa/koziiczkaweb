import {shallow, ShallowWrapper} from 'enzyme';
import React from 'react';
import {NavBarMobile} from '../../src/components/NavBarMobile';
import {POLISH, SPANISH} from '../../src/entities/Language';

describe('NavBarMobile', (): void => {
	it('renders closed', (): void => {
		//when
		const wrapper: ShallowWrapper = shallow(<NavBarMobile
			opened={false}
			language={POLISH}
			onChange={(): void => undefined}
		/>);

		//then
		expect(wrapper).toMatchSnapshot();
	});

	it('renders opened', (): void => {
		//when
		const wrapper: ShallowWrapper = shallow(<NavBarMobile
			opened={true}
			language={POLISH}
			onChange={(): void => undefined}
		/>);

		//then
		expect(wrapper).toMatchSnapshot();
	});

	it('changes language', (): void => {
		//given
		const handleChange: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(<NavBarMobile
			opened={true}
			language={POLISH}
			onChange={handleChange}
		/>);

		//when
		wrapper.find('#language-mobile').simulate('change', SPANISH);

		//then
		expect(wrapper).toMatchSnapshot();
		expect(handleChange).toHaveBeenCalledTimes(1);
		expect(handleChange).toHaveBeenCalledWith(SPANISH);
	});
});
