import {shallow, ShallowWrapper} from 'enzyme';
import React from 'react';
import {SectionTitle} from '../../src/components/SectionTitle';

describe('SectionTitle', () => {
	it('renders', () => {
		//when
		const wrapper: ShallowWrapper = shallow(<SectionTitle title={'test123'}/>);

		//then
		expect(wrapper).toMatchSnapshot();
	});
});
