import {shallow, ShallowWrapper} from 'enzyme';
import React from 'react';
import {SocialMediaTile} from '../../src/components/SocialMediaTile';
import LinkedIn from '../../resources/images/linkedin.svg';

describe('SocialMediaTile', (): void => {
	it('renders', (): void => {
		//when
		const wrapper: ShallowWrapper = shallow(<SocialMediaTile link='test' icon={<LinkedIn/>}/>);

		//then
		expect(wrapper).toMatchSnapshot();
	});
});
