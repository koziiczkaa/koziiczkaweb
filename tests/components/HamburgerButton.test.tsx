import {shallow, ShallowWrapper} from 'enzyme';
import React from 'react';
import {HamburgerButton} from '../../src/components/HamburgerButton';

describe('HamburgerButton', (): void  => {
	it('renders', (): void  => {
		//when
		const wrapper: ShallowWrapper = shallow(<HamburgerButton onClick={(): void => undefined} opened={false}/>);

		//then
		expect(wrapper).toMatchSnapshot();
	});

	it('renders opened', (): void  => {
		//when
		const wrapper: ShallowWrapper = shallow(<HamburgerButton onClick={(): void => undefined} opened={true}/>);

		//then
		expect(wrapper).toMatchSnapshot();
	});

	it('handles click', (): void => {
		//given
		const handleClick: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(<HamburgerButton onClick={handleClick} opened={false}/>);

		//when
		wrapper.find('#hamburgerbtn').simulate('click');

		//then
		expect(wrapper).toMatchSnapshot();
		expect(handleClick).toHaveBeenCalledTimes(1);
	});

	it('handles double click', (): void => {
		//given
		const handleClick: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(<HamburgerButton onClick={handleClick} opened={false}/>);

		//when
		wrapper.find('#hamburgerbtn').simulate('click');
		wrapper.find('#hamburgerbtn').simulate('click');

		//then
		expect(wrapper).toMatchSnapshot();
		expect(handleClick).toHaveBeenCalledTimes(2);
	});
});
