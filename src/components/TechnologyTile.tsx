import React from 'react';
import './TechnologyTile.scss';

export interface TechnologyTileProps {
	name: string;
	image: JSX.Element;
}

export function TechnologyTile(props: TechnologyTileProps): JSX.Element {
	return <div className='technology-item'>
		{props.image}
		<span className='technology-name'>{props.name}</span>
	</div>;
}
