import React from 'react';
import './HomeSection.scss';
import {Button} from '../components/Button';
import {Trans, useTranslation} from 'react-i18next';

export function HomeSection(): JSX.Element {
	const {t} = useTranslation();

	return <div className='home'>
		<div className='home-left'>
			<h5 className='home-title--s'><Trans i18nKey='home-small-title'/></h5>
			<h1 className='home-title--l'>Alicja Kozik</h1>
			<h3 className='home-title--m'><Trans i18nKey='home-large-title'/></h3>
			<p className='home-title--paragraph'><Trans i18nKey='home-about-me'/></p>
			<Button name={t('button-read-more')}/>
		</div>
		<div className='home-right'>
			<img className='home-image' src='../../resources/images/undraw.svg' alt='frontend'/>
		</div>
	</div>;
}
