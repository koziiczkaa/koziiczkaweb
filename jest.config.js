module.exports = {
    clearMocks: true,
    collectCoverageFrom: [
        '<rootDir>/src/**/*.{ts,tsx}'
    ],
    coverageDirectory: 'coverage',
    moduleFileExtensions: [
        'js',
        'jsx',
        'node',
        'ts',
        'tsx'
    ],
    collectCoverage: true,
    coveragePathIgnorePatterns: [
        '\\\\node_modules\\\\',
        '<rootDir>/src/entities/.*\\.ts',
        '<rootDir>/src/typings/.*\\.ts',
        '<rootDir>/src/index.tsx'
    ],
    coverageThreshold: {
        global: {
            statements: 85,
            branches: 85,
            functions: 80,
            lines: 85
        }
    },
    coverageReporters: [
        'json',
        'text',
        'html'
    ],
    setupFilesAfterEnv: [
        'jest-enzyme'
    ],
    testEnvironment: 'enzyme',
    testEnvironmentOptions: {
        enzymeAdapter: 'react16'
    },
    testPathIgnorePatterns: [
        '<rootDir>/test/dataProviders/.*\\.ts'
    ],
    snapshotSerializers: [
        'enzyme-to-json/serializer'
    ],
    setupFiles: [
        './tests/jest.setup.ts'
    ],
    roots: [
        '<rootDir>/tests/'
    ],
    moduleDirectories: [
        '<rootDir>',
        'node_modules',
        'src'
    ],
    moduleNameMapper: {
        '\\.(jpg|ico|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$': '<rootDir>/tests/__mocks__/fileMock.ts',
        '\\.(css|sass|less|scss)$': '<rootDir>/tests/__mocks__/styleMock.ts'
    }
};
